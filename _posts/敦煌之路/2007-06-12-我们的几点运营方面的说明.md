---
layout: post
title: "我们的几点运营方面的说明"
date: 2007-06-12 13:05:30 +0800
description: ""
categories: 敦煌之路
tags: [运营, 千寻]
lastmod: 
--- 


千寻网站在一开始之初，就定下了我们的道德底线，并欢迎广大网民监督！

1. 不采用任何流氓软件宣传方式宣传
2. 反对色情，这类东西私藏无伤大雅，公开传播我们将动用极刑
3. 我们鼓励同业相互借鉴，共同提高网民的用户体验
4. 我们支持以实际行动，在力所能及的范围内，在日常生活中点点滴滴中进行环保实践
5. 我们以打造用户更舒适、更简易生活品质为宗旨！我们更倾向于合作！

