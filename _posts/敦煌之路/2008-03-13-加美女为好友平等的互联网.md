---
layout: post
title: "加美女为好友：平等的互联网"
date: 2008-03-13 15:33:13 +0800
description: ""
categories: 敦煌之路
tags: [平等, 掩饰]
lastmod: 
--- 

> 我一直不喜欢“角色扮演”类游戏，一直不明白，一个人冒充某位大侠，在街上或者山谷中，走来走去或者飞来飞去，有啥意思。
> 
> 王乐告诉我：“**很多人玩RPG，是为了交友。**”在街上看到美女，顶多多看两眼，多半不会上去搭讪，因为多半会被骂流氓。**有了网络和游戏这块遮羞布，加美女为好友，成了天经地义、无可指责的事情。**
> 
> 史玉柱给巨人游戏里面的美女发工资，引发众多美女注册巨人游戏，巨人玩家便可以加到更多美女好友，美女在巨人里被追捧得乐不思蜀，巨人在线人数直逼征途。皆大欢喜。
> 
> 谁不高兴？2001年，我经常去广州，朋友开车来接我，老向我抱怨：“为什么烂仔总能泡到靓妞？”我听得不耐烦了，就问他：“你有时间整天聊QQ吗？”我另外一个深圳朋友，十分痛恨，“一个烂仔聊QQ将他的妹妹给‘拐’跑了。”我问他：“你聊QQ吗？”
> 
> **没占到便宜的、吃了亏的以及道德高尚的在抱怨、在痛恨、在谴责，占了便宜的在交流、在总结，在寻找着更好的地方“加美女为好友”。**劲舞团已经让高校门前，接美女下课的名车们在相互攀比了，拿了巨人工资的美女们不知道会整出啥动静。
> 
> 啥动静都不奇怪。孔老夫子早就说：“食色性也。”但他老人家一个人去见卫国美女南子，回来，子路不悦。先生指着天发誓：“我所行，若有不合礼不由道的，天会厌弃我，天会厌弃我。”那时，若有网络游戏，孔子也可以加南子为好友，子路没理由不悦，孔子没必要发誓。
> 
> 史玉柱总是很直接，他首先将玩家分成人民币玩家和非人民币玩家，现在又将玩家分成美女玩家和非美女玩家。他当然会被骂，但骂得越多，骂声越大，“巨人游戏=美女多”越会被强化。美女多的地方，人自然就多。
> 
> 炳叔写得很有意思：“史玉柱没有娶三宫六院的意思，他想的是，**男人都想，在虚拟的世界里，文明地、匿名地，陈，一把，冠希。**”所以，需要很多美女。


互联网对整个社会的改变深刻得远远超乎我们自己的想象力，网络也让这个世界变得越来越扁平。原来只能看表面的别墅、名车、身份之类的，在互联网里多了内心、互动、丰满，权贵原有的特权正在消解。
