---
layout: post
title: "鸡同鸭讲：扯的macbookair测评"
date: 2008-01-27 15:18:10 +0800
description: ""
categories: 技术视野
tags: [测评]
lastmod:
---

苹果在最近发布了超薄的macbook air，整个笔记本是装在一个信封里面的，结果，某位高手名为gizmodo的，立马拿来测评，而参与测评的竟然是macbook和mackbook pro，也就相当于测评华硕的“易pc”用的是笔记本的标准一样，一个掌上和一个用两只手托的，能比吗？可以，当然可以了，尤其是性能方面。当然，**结果也是显而易见的，手持的性能低劣。**但是，问题在于另一个方面，有点像田忌赛马一样，**我赛的是超便携，结果你不敢拿这个比，这就有点找茬的味道在里面了！**

相信很多人也都碰到过这种情景，也很多人是这样，因为现有的某个东西、某个模式很成功，于是就以这种模式为标准来衡量一切，就像现在很多人认为搜索引擎是互联网的核心领地一样，也很扯，之所以推崇搜索只不过是因为大家看到了成功的搜索，然后再为搜索的成功找出一堆的理由，其实大家并不知道搜索为什么成功，更不知道下一个成功的会是谁，反正现在有话题先忽悠着，占据高地再说，那管下一个成功会让现在的高地变成盆地呢！于是，大家纷纷专门找你明显的缺点说事，形成舆论压倒优势，但却根本**没考虑这个明显缺点换来的是另一个明显的优势！**而自己<mark>要做的就是把这优势扩大，并且让人们去接受这个优势带来的方便，而不是把那个明显的缺陷弥补！</mark>

这个问题同样发生在千寻上面，千寻网站的目的发展朋友关系，并让用户能获得因此带来的收益。因为前面做得是blog，结果被人认为是一个blog网站，然后各种意见全部集中在如何建一个blog网站上面，搞得自己很郁闷；包括首页，很多人的意见都是首页上放n多东西，以表明网站内容的丰富，促进所谓氛围的产生，也方便首页浏览者查看，否则就是不方便、就是不是给中国人用的、不考虑用户需要、不考虑用户感受……但**作为促进友情的网站，更多的是属于私密性的东西，放首页这种公共地方，简直就是无视用户的隐私，是对用户的一种冒犯**。所以真正的首页应该不存在这些属于私人圈子的东西。更多的是介绍性的。千寻以后的首页应该向这个方向努力，而不是相反，虽然这样这类批评意见永不断续……

## 新闻原文： ##

Gizmodo在拿到MacBook Air后，进行了全球首批性能测试。参与此次测试的除了MacBook Air以外，还有老一代的MacBook和MacBook Pro。
本次测试运行了4个测试程序，分别从音频解码能力、视频解码能力、与外部储存设备的传输速率、复制文件速率等方面对Air进行考察。

参与测试的3台笔记本配置如下：

+ The **MacBook Air** has a 1.6 GHz custom Intel processor, 2 GB 667 MHz DDR2 RAM, and an 80 GB, 1.8", 4200 RPM HDD.
+ The **MacBook** has a 2 GHz Intel Core Duo processor, 1 GB 667 MHz DDR2 RAM, and a 120 GB, 2.5", 5400 HDD.
+ The **MacBook Pro**  has a 2.2 GHz Intel Core Duo processor, 2GB 667 MHz DDR2 RAM, and a custom 160 GB, 2.5", 5400 RPM, Seagate Momentus HDD.

下面是测试结果：

| 项目  |  MacBook Air | 	MacBook |  MackBook Pro |	Notes |
| --- | --- | --- | --- | --- |
| MP3 Encode Test	| 3m14s	| 3m51s	| 3m40s | Quicker is better on all tests |
| Video Convert Test |	 1m13s	| 1m11s 	| 48s |  | 	 
| Thumbdrive to MacBook Test	| 35s |	33s |	29s |  	|
| Thumbdrive Duplicate Test	| 1m15s |	1m03s |	1m00s | | 	 
| Boot Test	| 45s | 	41s 	| 30s |  	 |
