---
layout: post
title: "calligra suite已然成熟，可以替换libreoffice了"
date: 2013-09-13 13:00:28
description: "使用jekyll生成静态站"
categories: 技术学习
tags: [办公软件]
lastmod: 2014-10-11
---

昨天特意打开了calligra，和libreoffice对比了下。

+ 文档编辑部分：calligra words 和 libreoffice writer比，对doc和docx的渲染方面，calligra优于libreoffice，对其中表格的操作远远优于目前所有已知的办公软件，操作流畅。但calligra欠缺一个重要功能：文档结构图。
+ 幻灯片部分：两者对ppt的渲染差不多，calligra更快，同是加载30m的文档，libreoffice崩溃，calligra正常。但calligra对odp的渐变色渲染，图形定位还有问题。
+ 表格部分：没仔细对比过，不知。
+ 数据库部分：对这两者的数据库都不用，所以具体不知，只知道calligra界面简化。
+ 绘图部分：calligra完胜libreoffice，可以媲美的是gimp和inkscape。
+ 流程图部分：libreoffice没有，calligra flow相当于微软office里面的visio，图标较全，应该可用。


总结归纳下：calligra已然成熟可用了，替换libreoffice的可能性大增。

另外，虽然我一直主张基于浏览器创建办公套件，但calligra创新了一系列的东西，还是很值得期待。libreoffice，微软办公套件，wps都已老迈。
 

附加：

我对doc都是只读取，保存就换odt。

比如ribbon菜单栏，可取的也就图标化直观，但却带严重设计缺陷：小的地方太小，大的地方太大，太占用屏幕顶部，尤其是宽屏普及的情况下，严重影响视野，另外就是图标混乱。倒是好多年前的wps，做过有很多有意思的创新，比如以前一个拨号器就实用且美伦美涣，让人爱不释手，可惜这些东西早就被抛弃，彻底来了一个抄！

 