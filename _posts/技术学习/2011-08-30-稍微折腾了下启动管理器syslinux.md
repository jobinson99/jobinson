---
layout: post
title: "稍微折腾了下启动管理器syslinux"
date: 2011-08-30 14:09:04 +0800
description: ""
categories: 技术学习
tags: [启动管理器]
lastmod: 
--- 

基于个人非常懒的事实，其实也没折腾什么，就是用syslinux替换掉我的grub1，顺手把archlinux的相关维基部分也给翻成中文了。

## 一、为何选syslinux： ##

其实呢，对于基本系统，我是不太想去折腾的，不过时代的脚步声在我耳边聒噪不停，为了彻底解决问题，就顺便鼓捣下这个东东。

初看起来syslinux比grub的配置复杂了点——这是废话，可使用图形启动的不复杂才怪。

我原来想，如果不是因为syslinux 支持btrfs，grub1才是性价比最高的一个系统启动管理器：小巧、够用，不多不少恰恰好。当然，我这想法是因为我不太喜欢花俏，或者说，这是基于系统管理员的角度，如果基于普通人日常使用的角度而言，那么grub该进坟墓。——这其实是误解，syslinux模块化的设计，也可以很小巧。

syslinux的复杂和grub2比起来，也是相对简单的一个。

但如果全程的图形化动画化：syslinux+？+kdm+ksplash+……这个启动过程虽然漂亮，但确实繁冗得可以。——什么，我现在流行用“挂起”，不再需要这么漂亮的东西，哈哈，其实这就是现实，面对过分的繁华，大家用脚投票，直接来个更简化的“挂起”。

因此，我其实没必要花那么多精力在这个syslinux里面。

## 二、syslinux的安装： ##

archlinux下比较简单：

    pacman -S syslinux

但安装完不代表就完事，还需要两个步骤：

1. 安装进启动卷区
2. 安装所需的特定模块

archlinux本身就提供了一个自动化的脚本syslinux-install_update处理这些事，这脚本已经比较全，如果不想浪费时间去定制，就用这脚本来完成整个过程吧。

## 三、syslinux的配置： ##

1、最简单的方式：

syslinux是模块化的，需要什么，就把相应模块移入到启动目录里——也因此，可以说，为了最简单化，完全可以什么模块都不要，以类似于grub1的方式最简启动，这回不用纠结于grub1的远去了：

    PROMPT 1
    TIMEOUT 50
    DEFAULT arch
    
    LABEL arch
    LINUX ../vmlinuz-linux
    APPEND root=/dev/sda2 ro
    INITRD ../initramfs-linux.img
    
    LABEL archfallback
    LINUX ../vmlinuz-linux
    APPEND root=/dev/sda2 ro
    INITRD ../initramfs-linux-fallback.img

2、稍微不简单：使用文本菜单
配置文件不帖了

3、更复杂点：图形化界面
配置文件更复杂了，也不帖了。

这里只提下如何制作syslinux背景图：640×480，制作成png，后转为16色png

    convert -depth 16 -colors 65536 xxx.png splash.png
    sudo mv splash.png /boot/syslinux/splash.png

## 四、最终结果 ##

整个 */boot/syslinux/syslinux.cfg* 的文本有如下特点：

1. 所有配置的名称都是大写，配置的值可大写也可小写。比如DEFAULT arch，就是默认使用标签为arch的系统启动，这点和grub不一样，grub整个配置文件都是小写的。
2. 启动菜单项都是以LABEL为起始，LABEL之后是该项相关配置。
3. 文本结构：
    1. 文本开始定义的是基础的启动项目：是否使用文本菜单，何时超时自动启动，默认启动哪个系统。
    2. 接下来是启动界面的配置：如果仅仅是使用文本，则配置文本菜单的界面，如果启用了图形启动界面（vesamenu），则需要配置图形部分
    3. 最后是各个启动项的配置参数

了解了文本结构和特点之后，感觉其实很简单的一个东西，上面真是没必要这么多废话。


后记：

半天后换成grub2,因为grub2支持中文,并且，grub2支持直接挂载镜像文件。

