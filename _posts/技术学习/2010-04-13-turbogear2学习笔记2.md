---
layout: post
title: "TurboGears2.1（极限动力2）学习笔记二_筋骨皮风格"
date: 2010-04-13 01:18:14 +0800
description: ""
categories: 技术学习
tags: [极限动力2, 筋骨皮, mvc, turbogears]
lastmod: 
--- 

TG2（恩，响应号召，就叫“极限动力2”吧）使用MCV设计风格（找不到好的中文翻译，就叫“筋骨皮”设计风格吧，呵呵，没错，是取自“人活一口气，树练筋骨皮”）设计，为什么需要筋骨皮分离呢？这个有维护、分工效率方面方便的原因。

一般来说，分工效率高，但如果团队太小，那么就要学早期生产塑胶花时候的李超人吧，什么事情都一个人搞，筋骨皮全部自己一双手来，这是没有办法的，你想咋样就咋样。

接下来，随着参与开发的人多了之后，就需要有初步的管理体系（恩，我们姑且叫svn吧，又一个缩写词，还好，这个有个名字叫 成效集中管理），好处不消说，就是分工负责，而且还不能相互冲突，此时，如果再像起始阶段一样，所有东西自己一个人来，就会有分工不明，导致混乱，所以，就有必要把东西拆成若干部分了，此时，就是我们说的筋骨皮风格就诞生并大大发挥效用了（注意：筋骨皮模式不是必须的，而是根据需要发展出来的，而且，还不是终点，随着事情的发展，很多需求会发展出来，更有效的方式也会被发明出来，比如现在就有的KISS就叫“简古纯粹”吧）。

这些东西，对于接触过一些和管理方面知识的人，都不会有任何异议的，有异议的是不同的具体实现方式。

在程序开发方面，古老的形式，比如我大学时学习的VFP，以数据库为中心，可以构建起任何一个程序（恩，确实是任何，至少我用VFP整过一个拨号程序，当年还需要56k猫 :_)）。但这种形式，随着数据库的发展，网络的兴起，人们渐渐发现，数据库种类多了，相互之间转换不方便，集中管理模式维护起来不方便等，使得把数据库部分再进行切分很有必要，于是，出现了筋骨皮这种模式（不管是mtv还是mvc，都是这个思路之下的产物，呵呵）

采用筋骨皮方式，那么首先会碰到一个问题，就是骨架部分。

在开始学习的时候，我很不习惯，因为我当年学习数据库的时候，以及后来搞经济社会统计的时候，是所有东西一次性设计完成，效率很高，而筋骨皮方式把各个部分分成了几个部分了，很不习惯，也感觉很混乱。

在学习极限动力2的同时，我还看了web.py，对其对骨架部分的做法很欣赏，因为够简洁，也相对符合我以前搞数据库的方式。但仔细一想，则发现这种直接操作数据库的方式，不利于维护，也不利于数据库转移，只能适合数据一经录入就用不变动的，并且数据库结构以后一直都能用的统计里面，而不适合需要变动数据结构、转移数据库的网站中——所以，需要一个中介层（叫ORM，这个如何翻译呢？数据库映射，数据库兼容接口？），这个中介层不再管数据库字段里面关于操作、显示方面的因素，只做和骨架有关的，这样可以在不影响目前运行服务的情况下，在线下对数据库结构进行重新设计。

**数据库 ∈ { 存储文件，数据库结构(schema)，数据库可用操作 }**

数据库映射负责的是数据库结构部分。具体看极限动力2的做法，或者说是SQLAlchemy的具体做法就是：

把原来数据库VFP时代中的字段标题，字段类型格式限定等，都分别放在controller或者template里面。

只保留（比较泛泛）：

+ 字段名＋类型＋长度＋是否自增长＋默认值＋是否可空＋外连键＋说明（对于数据库本身基本没啥价值，但对于维护数据库结构来说，还是很重要的东西）
+ ＋初始值（这个可以不在创建骨架的时候生成，在骨架完成后，直接对数据库操作。不是默认值，默认值是属于外观的，可以不写入数据库。）
+ ＋对数据库的操作，放到筋(controller)中
+ ＋对数据库结果展示，放到皮(template)中

总算弄明白了。举个例子：

原来电子邮件，规定了具体的格式，而在筋骨皮模式中，

A、数据库列是泛泛规定的，以方便在不同数据库间转移，同时也方便数据库结构修改，不至于影响操控和外观部分。比如可以直接存储为string或者unicode

B、对电子邮件的格式的规定，是通过controller的验证部分来实现的，不符合规定的就拒绝接收。这样无需直接对数据库进行操作，有效降低数据库的负载，直接在相对数据库较快／省资源的验证部分就搞定了。而数据库也因此获得巨大的数据灵活性，方便数据库间数据转移。

C、为了方便理解，有时有必要显示一个电子邮件的默认值作为参考示例，比如admin@admin.com，如果是古老的数据库，一般方法是直接在数据库中定义默认值（在统计里面，默认值是非常重要的，可以减少大量的重复性输入）。而实际上，这个值完全没必要写入、也完全没必要从数据库读出，完全可以在筋骨皮模式中的皮部分搞定，和B一样，也是**把没必要和数据库交互的部分完全剥离出了骨架部分。**


