---
layout: post
title: "选cms和技术门槛"
date: 2008-10-24 21:34:27 +0800
description: ""
categories: 技术学习
tags: [内容管理系统, 选择]
lastmod: 
--- 

最近在同时做4个网站，自己技术更多的是了解，而不是自己动手，所以对我来说，这任务重了点，但从实际操作来看，其实也不算重，只是自己一开始的目标有点过高，原定是8天搞定，现在8天已过了，只有一个还算可以的网站出来，而出来的这个也将被我彻底否决掉。

因为有非常优秀的各种cms，使得我建站成本不高，因而拉升了我的期望值，而且我对cms本来也只是粗浅了解，直到自己试手下来，才开始有点感觉。

总结几点吧：

1、如果想又快又好地建设网站，那么请选cms。

2、接下来是选：首先不是去直接玩那个cms，这会浪费很多时间的，我就是因为一开始这样，现在后悔时间浪费了。

选cms，**首先要看的不是cms本身，而是要看cms的扩展主要集中在那方面！**

比如如果要做一个设群类网站，选joomla会有点郁闷，因为joomla本身这方面欠缺，而且扩展就一个开发中的groupjive，基于groupjive的扩展几乎没有。

选cms，首先看的不是cms，而是其扩展的量和集中度以及开发热度，joomla目前集中开发的是基于个人的应用（groupjive还在开发中，其他用户管理的不是要hack主程序就是商业，而且还不咋样），所以我碰了很大的壁，即使我想过并且尝试了利用网店、wiki、project、tag类扩展来改装，都无功而返！

第二，看这些扩展统合起来是不是和自己的需求符合，如果是，那么可以进行下一步了。

第三步选cms，此时要看的是cms的扩展开发热度，热度越高，问题越好解决。

3、**选cms版本！**这个也困扰了我很久，因为我的习惯是选用最新的，但最新的cms往往扩展跟不上，所以版本跃升效用没那么明显，要延后好几年才能显现这新版本的价值，比如drupal5就已经有12个修订版了，而drupal6至今6个版本，却还没完全把drupal5的扩展完全迁移过来

记住！网站是给你和其他人用的，要好用才行，功能不到位，再新也白搭。

4、二次开发，二次开发的话，如果扩展功能已经满足要求，剩下的最大的问题是整合！怎么把各种功能整合在一起，这包括外观和语言

5、语言重要吗？不重要！因为自己做网站，往往很多用语都会自己定义，所以只要看得懂后台就行，前台的东西自己肯定不会依赖现有的cms翻译

6、皮肤

选cms建网站最大的问题归结为一个**“猴子下山的问题”**，细分为三个：

1. **挑花了眼：**因为太多优秀的cms了，而要熟悉一个都需要花点时间，尤其是熟悉其扩展的情况，所以，一旦认定，切莫轻易动摇！时间宝贵，对一个研究得深了，很多问题和很多外部诱惑迎刃而解！
2. **选扩展**，这是个时间问题，最好是一个一个调试，而不是一口气吞入，一口气吞入往往系统负载出问题，而且整合也肯定搞的肤浅，还因为规模过大，超过自己的处理能力把自己搞懵；
3. **整合**：因为网站是组装起来的，外观和内部沟通多多少有问题，此部分也要专！在一个确定的主皮肤上花点时间整合，一套完美了，以后要再多搞皮肤就驾轻就熟了！

相信绝大部分人对科技，尤其是应用普及的互联网相关技术，是望而且步的，我想大部分的缘由，不是没那个能力和时间去懂，而更多的是心理和思维障碍，**尤其是不敢犯错的心理**，稍微一点操作不顺，就不敢进一步了，不说传统的软件行业，单就互联网这个行业，其实已经没有必要去懂很多东西就能玩得通了，因为有各种界面友好的网站，把各种复杂的技术变成简易容易操作、而且还非常友好生动活泼有趣！


