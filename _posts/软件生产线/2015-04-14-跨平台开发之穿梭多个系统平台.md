---
layout: post
title: "跨平台开发：穿梭多个系统平台的统一开发环境【初探】"
date: 2015-04-14 22:34:19 +0800
description: ""
categories: 软件生产线
tags: [跨平台开发, 虚拟机, 标箱]
lastmod: 2015-11-09
--- 

## 情况 ##

刚好接了朋友一个烂尾工程，工程用.net开发的，很乱。

项目经过分析之后，因为考虑到还需要构建两个适用于 安卓和苹果 系统的应用，未来可能还需要更多的应用编译。发现其实可以：.net继续后台，前台全部换用js。

如果，后面如果还是我负责，那就全换了。

## 我的需求和遇到的问题 ##

遇到的问题：win实在太慢了，配置那么高级的电脑用各种软件却比我十年前标准配置的电脑还慢。——虚拟机里的linux倒是很快。

window下搞开发实在很不方便，虽说有各种整合开发环境，但还是各种欠缺欠缺，比如iojs之类的构建、一些命令行下才可用的包，emacs之类的也很不方便……太多需要配置了。

还有另一个让我头疼的问题：由于开发用的电脑不是我的，没法重装系统，项目完后要还回去。

需要：本人已经好多年在linux下生存，所有的生态链全部是用linux的，我需要用命令行，需要用软链，需要用emacs等，我需要统一整齐的管理文件，我不想用盗版软件！！！

经过几天郁闷的双电脑+双手机开发后，终于想到办法，解决这个 穿梭在各个平台设备开发的问题。

## 我需要实现的功能： ##

我的设备：一个手机（应该是两部，一部刚好换掉了），两台电脑（一台自己多年用的，所有东西都在上面，但配置低；一台是项目的，配置前沿，里面已经备有各种相应开发工具，但不适用于我）

工作方式：平时开发只用一台电脑（当然是性能最好的那台），碎片时间用手机，在这台电脑和手机里，使用我熟悉和方便的工具，同时，收集到的经验，可以同步到我自己的电脑上。

需要实现的功能：三机同步。


## 具体解决方式 ##

自用电脑：开发所需的东西，全用 git管理起来，可公开的全部github，不好公开的全部bitbucket或者gitlab 私库。

项目的电脑：尽可能少用其本身软件，只用虚拟机，并且把虚拟机快照化，这样可以快速启动所需的环境。

为了以备不时之需，window还装了cygwin+emacs。

msys2+emacs

软件：
cygwin和虚拟机都装了git 和 emacs，emacs配置全部拷自我自用电脑。


更多探索：可自定义一个 **某人开发专用 标软箱** 一劳永逸解决。跨平台需要使用的虚拟机vbox或者qemu，配置则有三种选择：

- vbox导出镜像+说明：安装好了，稍微配置下。
- vagrant镜像+配置：超大，因为win下还需要安装 vagrant和ssh工具。也是系统安装好了，稍微配置下。
- nixos或者类似一体化系统：最小！！需要346M的安装镜像。安装时间难说（因为网速！！！）。

手机：一个编辑器+一个可管理git的工具。

## 项目用电脑 ##

### 初探：使用 cygwin 试图和win融合 ###

一开始是想到用cygwin或者是cygwin的集成版babun来实现，配置另文表达。

摸索了几天cygwin后，感觉与其在一台电脑上构建一个cygwin，还不如直接虚拟机+快照，或者闪盘系统呢！！！这样完整且统一。

而且，配置多了后，如果不启用伺服模式，cygwin速度非常慢，比vbox快照慢多了。

另外，cygwin的软件管理工具才刚起步，没有 类似 archlinux的 用户自治资源区（aur），没法方便查找和编译各种软件。

### 虚拟机模拟 ###

如果是小系统拷到内存中，反正现在内存够。

平台规划：

- 开发用：nixos（开发需要底层版本可控+多版本共存，一开始选择了gentoo系，后来遇到nixos），桌面系统使用 lxde 或者其他小型窗口管理器。开发环境emacs，编译环境node.js cordova
- 运行应用用：
    - 给php的：本来想用coreos，无奈网络不通！！！！所以只能作罢）或者其他极简部署用系统，其他可选 ubuntu server 或者 centos等，运行php类，java类，
    - 给 .net的：win 2008 server运行.net类，主机极致化。
- 编译测试需要：
    - 安卓虚拟机
    - ios


#### vbox模拟 ####

##### 准备工作： #####

- 看看电脑主板是不是开启支持 硬件虚拟技术（Hardware Virtualization Technology (VT)），没开启就到bois开启。
- [vbox下载地址](https://www.virtualbox.org/wiki/Downloads)，根据宿主机操作系统类型选择下载。另外，还需要扩展下载，也就是那个[VirtualBox 4.3.26 Oracle VM VirtualBox Extension Pack]()


本人的宿主机是win7，安装vbox有问题，解决办法：

+ 路径不含中文名
+ 恢复系统主题

##### 安装操作系统 #####

可以直接安装，也可直接下载已经安装好的系统镜像，节省时间。

- [下载已有的镜像](http://virtualboxes.org/tag/ova)
- [搜镜像种子](http://kickass.to)
- [开源镜像站](http://mirrors.neusoft.edu.cn/android/repository/)

##### 配置强化虚拟机系统 #####

安装完成后，就可以用了，但为了更方便使用，需要进行下面的配置，其中重点是配置：文件共享和网络服务。

##### vbox虚拟机里的linux配置 #####

默认安装就可以进入虚拟系统了，想要更多功能，需要进行下面配置。

##### 虚拟机 archlinux 各种服务配置： #####

安装 virtualbox-guest-modules virtualbox-guest-utils virtualbox-guest-dkms

按安装后的提示，生成模块。

然后，加载模块：

    modprobe -a vboxguest vboxsf vboxvideo

启动服务

    systemctl enable vboxservice

也可以手动启动vbox的各种服务：比如双向粘贴，双向拖动文件，无缝模式……

    VBoxClient

也可以一次启动所有 

    VBoxClientAll

让其启动即可使用这些服务：不过不建议这么做，因为出问题的时候，就没法救。如果有桌面，把VBoxClientAll 加入到autostart中，如果没有，就加入到  `.xinitrc`

### 网络设置 ###


##### 文件共享设置： #####

##### 自动加载 #####

把用户加入到vboxsf组中。

    gpasswd -a $USER vboxsf

查看下已经存在的共享目录

    VboxControl sharefolder list

使用 `VboxControl sharefolder` 自动加载。

如果：仍然无法自动加载，可手动新建一目录：/media/sf_共享文件夹名字

##### 手动加载 #####

也可以手动加载：

    mount -t vboxsf 共享文件夹名 挂载点
    比如：
    mount -t vboxsf -o uid=1000,gid=1000 home /mnt/

或者：

    mount.vboxsf

注意加载文件夹的权限：755

软链到桌面，方便使用。

    ln -s /media/ ~/my_documents

启动时自动加载：修改fstab

    xxx   /media/xxx    vboxsf  uid=user,gid=group,rw,dmode=700,fmode=600,comment=systemd.automount 0 0

换漂亮点的图标

[kde4.6之前的图标好看，不足的部分应该可以用之后的图标补上，我fork了一个，同时补上这个版本后更新的图标](https://bitbucket.org/jobinson99/oxygen4-icons)



#### mac os虚拟机安装手记 ####

macos 软件安装模式：拖入图标到Application，复制完成后，打开 Lauchpad ，点击该图标，根据提示安装。

xcode挺大的！


##### win + vbox 安装macos #####

- [win7 vbox 安装 mac os 10.11 参考](http://bbs.feng.com/read-htm-tid-9908410.html)
- [win7 vbox 安装 mac os 10.10 参考](http://bbs.feng.com/read-htm-tid-8474315.html)[全新安装](https://xuanwo.org/2015/08/09/vmware-mac-os-x-intro/)
- [win7 vbox 安装 mac os 10.9 参考](http://bbs.feng.com/read-htm-tid-7625465.html)
- [win7 vbox 安装 mac os 10.7 参考](http://bbs.zol.com.cn/nbbbs/d160_148114.html)

调整虚拟机系统分辨率：
在引导界面，添加 `"Graphics Mode"="1366x768x32"`

需要引导工具

下载镜像后，选择导入即可。

因为版本本身不稳定（比如点击 Apple icon –> About This Mac，系统会崩溃），需要用 sw_vers 检查下版本：在启动界面，使用键盘快捷键Cmd + Space，开启命令行。（此处注意不要点按钮，会崩溃的）

键盘输入问题：安装 [DoubleCommand](http://doublecommand.sourceforge.net/)，配置适合自己的输入方式。

然后[注册苹果账号](http://developer.apple.com/programs/register/)，下载Xcode  和 iOS SDK。

打开工程项目文件  hello/platforms/ios/hello.xcodeproj


### qemu模拟 ###

### win上用qemu ###

### linux上用qemu ###

#### qemu模拟linux ####

##### linux + qemu 安装macos #####

- [参考](http://blog.definedcode.com/osx-qemu-kvm)
- [参考](http://www.contrib.andrew.cmu.edu/~somlo/OSXKVM/)


### linux系统下的ios开发环境/cygwin也类似


## 安卓手机开发测试环境配置 ##

- [查找应用](http://www.apkpure.com)
- [查找应用：可下载](http://www.apkmirror.com)
- [查找应用](http://www.apk4fun.com)
- [查找应用：可下载](http://rexdl.com/)
- [查找开源应用](http://www.f-droid.org)
- [查找应用](http://www.appsfinder.fr)


### 在手机上运行各种应用 ###

有空可探索：

- android + linux deploy 或者 debkit：需要root
- Android + Linux Chroot + Node.js
- firefoxos + 
- JXCore One of the key features in the recent release was the ability to run Node.js applications on mobile, both Android and iOS, while leveraging the full Node.js ecosystem on npm.

除去直接硬刷linux系统到手机上外，还有四个软办法：

- 一是获得root权限，然后安装busybox之类的。
- 另一个是编译安装 一个包管理器（等有空折腾debian的dpkg），
- 比较方便的是 安卓上的编程环境 terminalIDE。
- 比较自由的：不用root，直接安装一个linux到手机上：gnuroot

英文输入键盘：手写用 writepad

英文虚拟电脑键盘用：terminal ide的键盘

#### terminal ide ####

terminal ide帮助比较全面，可以当linux 操作入门。

生成 身份认证：和linux上的ssh-keygen不同。

    mkdir ~/.ssh        
    dropbearkey -t rsa -f ~/.ssh/id_rsa
    dropbearkey -y -f ~/.ssh/id_rsa | grep "^ssh-rsa" >> my_keydropbearkey -y -f ~/.ssh/id_rsa | sed -n 2p > ~/.ssh/id_rsa.pub

第一条命令是新建文件夹，第二条命令是 生成 私钥，第三条是本地安全管道开启密钥认证，第四条是把公钥拷到 *my_key* 上，然后把 *my_key* 拷到所需的git服务器上。

修改下本地的配置

新建一文件 ~/sssh （名字随意，只要下面相应改下对应的），内容如下：

    #!/data/data/com.spartacusrex.spartacuside/files/system/bin/bash
    exec ssh -i ~/.ssh/id_rsa "$@"


修改下执行权限和修改配置文件 ~/.bashrc

    chmod 755 ~/sssh
    echo export GIT_SSH=~/sssh >> .bashrc

到这步搞定！

### 三平台多终端开发环境搭建 ###

实质是两种：本地编译（编译的工具链和编译量都很大！）或者两个虚拟机（linux和mac os）里的cordova编译

另文说明。

### 更多探索 ###

其实这样还是不够方便的，除了开发，测试也需要类似的环境，所以，最后探索的应该是 容器类工具。

### 附带几句牢骚：

这几天被苹果系统问题卡住了所有的进度了：不是因为技术难度高，而是烦，尽搞些没用的限制——就有点像当年某些游戏用了俄国某种特别变态的加密程序，导致正版玩家体验也严重不爽一样，纯粹就是防贼防过头了。

你牛叉系统可以说是为了提升效率，所以虽然入门有点难度，但经历过入门的不适应期后，简直如鱼得水。

微软的系统可以说是为了适合一般人使用电脑，是以易用为追求的——虽然这个“易用”实在做得惨不忍睹。

苹果呢？号称是为了设计师，可是再怎么强大的苹果电脑，再怎么定制的软件，有 电脑+云渲染牛叉么？

苹果操作系统是专用系统？为最大化发挥苹果电脑硬件威力而造？是为了自己兼容性差找接口吧？苹果的根bsd也是高大上，可人家兼容性一点也没荒废，甚至还有号称兼容性举世无双的netbsd，人家也不难用，更不要说难装到其他系统上。

设定了一堆没必要的门槛，拦的是什么人呢？好像是自己用户，制造了一堆门槛，让没事干的人去琢磨，去消耗时间，获得一定的成就感，然后用来炫……这用户心理实在把握得不错。

结合苹果的娱乐消费倾向，还真的是一脉相承。

